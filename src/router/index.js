import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";
import baseUrl from "../utils/baseUrl";

import { getCookie } from "../utils/utils";

Vue.use(VueRouter);
// const originalPush = VueRouter.prototype.push
// VueRouter.prototype.push = function push(location) {
//   return originalPush.call(this, location).catch(err => err)
// }
const routes = [{
        path: "/",
        component: () =>
            import ("../views/Bank.vue"),
        meta: {
            isFooter: true,
            requireAuth: true,
            title: "超维编程大使",
        },
    },
    {
        path: "/auth",
        name: "auth",
        component: () =>
            import ("../views/Auth.vue"),
        meta: {
            isFooter: false,
            requireAuth: false,
            title: "超维编程大使",
        },
    },
    {
        path: "/callback",
        name: "callback",
        component: () =>
            import ("../views/Callback.vue"),
        meta: {
            isFooter: false,
            requireAuth: false,
            title: "",
        },
    },
    {
        path: "/income",
        name: "income",
        component: () =>
            import ("../views/Income.vue"),
        meta: {
            isFooter: true,
            requireAuth: true,
            title: "我的收益",
        },
    },
    {
        path: "/cashinfo",
        name: "cashinfo",
        component: () =>
            import ("../views/CashInfo.vue"),
        meta: {
            isFooter: false,
            requireAuth: true,
            title: "提现记录",
        },
    },
    {
        path: "/login",
        name: "login",
        component: () =>
            import ("../views/Login.vue"),
        meta: {
            isFooter: false,
        },
    },

    {
        path: "/mygroup",
        name: "mygroup",
        component: () =>
            import ("../views/MyGroup.vue"),
        meta: {
            isFooter: true,
            requireAuth: true,
            title: "我的团队",
        },
    },
    {
        path: "/guestlist/:id",
        name: "guestlist",
        component: () =>
            import ("../views/GuestList.vue"),
        meta: {
            isFooter: false,
            requireAuth: true,
            title: "他的团队",
        },
    },
    {
        path: "/group",
        name: "group",
        component: () =>
            import ("../views/Group.vue"),
        meta: {
            isFooter: true,
            requireAuth: true,
            title: "成为团队长",
        },
    },
    {
        path: "/addus",
        name: "addus",
        component: () =>
            import ("../views/AddUs.vue"),
        meta: {
            isFooter: false,
            requireAuth: true,
            title: "加入我们",
        },
    },

    {
        path: "/article",
        name: "article",
        component: () =>
            import ("../views/Article.vue"),
        meta: {
            isFooter: false,
            requireAuth: true,
            title: "赚钱宝典",
        },
    },
    {
        path: "/instructions",
        name: "instructions",
        component: () =>
            import ("../views/Instructions.vue"),
        meta: {
            isFooter: false,
            requireAuth: true,
            title: "使用说明",
        },
    },
    {
        path: "/articlelist",
        name: "articlelist",
        component: () =>
            import ("../views/ArticleList.vue"),
        meta: {
            isFooter: false,
            requireAuth: true,
            title: "赚钱宝典",
        },
    },
    {
        path: "/cash",
        name: "cash",
        component: () =>
            import ("../views/Cash.vue"),
        meta: {
            isFooter: false,
            requireAuth: true,
            title: "我要提现",
        },
    },
    {
        path: "/articledetail/:id",
        name: "articledetail",
        component: () =>
            import ("../views/ArticleDetail.vue"),
        meta: {
            isFooter: false,
            requireAuth: true,
        },
    },
    {
        path: "/skill",
        name: "skill",
        component: () =>
            import ("../views/Skill.vue"),
        meta: {
            isFooter: false,
            requireAuth: true,
        },
    },
    {
        path: "/classsystem",
        name: "classsystem",
        component: () =>
            import ("../views/ClassSystem.vue"),
        meta: {
            isFooter: false,
            requireAuth: true,
        },
    },

    {
        path: "/user",
        name: "user",
        component: () =>
            import ("../views/User.vue"),
        meta: {
            isFooter: true,
            requireAuth: true,
        },
    },
    {
        path: "/invite",
        name: "invite",
        component: () =>
            import ("../views/Invite.vue"),
        meta: {
            isFooter: false,
        },
    },
    {
        path: "/group",
        name: "group",
        component: () =>
            import ("../views/Group.vue"),
        meta: {
            isFooter: false,
        },
    },
    // 扫码登录
    {
        path: "/invitation",
        name: "invitation",
        component: () =>
            import ("../views/Invitation.vue"),
        meta: {
            isFooter: false,
        },
    },
    {
        path: "/continue",
        name: "continue",
        component: () =>
            import ("../views/Continue.vue"),
        meta: {
            isFooter: false,
        },
    },

    {
        path: "/usersetting",
        name: "usersetting",
        component: () =>
            import ("../views/Usersetting.vue"),
        meta: {
            isFooter: false,
            title: "设置",
        },
    },

    {
        path: "/gopay",
        name: "gopay",
        component: () =>
            import ("../views/Gopay.vue"),
        meta: {
            isFooter: false,
        },
    },

    {
        path: "/updname",
        name: "updname",
        component: () =>
            import ("../views/UpdName.vue"),
        meta: {
            isFooter: false,
        },
    },
];

const router = new VueRouter({
    mode: "history",
    // 二级目录
    base: process.env.NODE_ENV === "production" ? `/${baseUrl}/` : "/",
    routes,
});

export default router;
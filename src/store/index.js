/*
 * @Author: your name
 * @Date: 2020-06-11 14:47:57
 * @LastEditTime: 2020-07-29 11:55:41
 * @LastEditors: your name
 * @Description: In User Settings Edit
 * @FilePath: \distrbute\src\store\index.js
 */ 
import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    isLoading: false

  },
  mutations: {
    updateLoadingStatus(state, payload) {
 
      state.isLoading = payload.isLoading
    },
  },
  actions: {
  },
  modules: {
  }
})

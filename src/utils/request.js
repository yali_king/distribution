/*
 * @Author: your name
 * @Date: 2020-06-11 14:47:57
 * @LastEditTime: 2020-07-29 17:13:27
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \distrbute\src\utils\request.js
 */

/**
 * 封装请求
 * @url: String 请求地址
 * @method: String 请求方法
 * @params: Object 请求参数
 */
import store from '../store/index'

import $ from "jquery"
import {
    getCookie
} from './utils'
const base = 'https://cvccode.com/distribution_api/user'
// const base = 'http://192.168.110.110:8084/distribution_api/user'


export function request(params) {
    const _this = this
    const token = getCookie('openid')
    const {
        url,
        method,
        data
    } = params
    let headers = {
        'Content-Type': "application/json;charset=utf8",
    }
    if (token) {
        headers.token = token
    }
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "POST",
            url: `${base}${url}`,
            data: data,
            method: method,
            headers,
            dataType: "json",
            success: function (res) {
                store.commit('updateLoadingStatus', {
                    isLoading: false
                })

                //  if(res.code==104){
                //    fetch('https://cvccode.com/chaowei_distribution/user/test?='+window.location.href)
                //  }
                resolve(res)


            }

        });

    })
}
import {
    request
} from './request'
import qs from 'qs'
import axios from "axios";

// 提现记录
export async function cashInfo(page) {
    return await request({
        url: `/get_cash_out?page=${page}`,
        method: 'get'
    })
}
// 生成二维码
export async function getQrCode(data) {
    return await request({
        url: "/qr_code",
        method: 'get',
        data
    })
}
// 成为团队长
export async function getOneNews() {
    return await request({
        url: "/article_detail?id=67",
        method: 'get'
    })
}
// 我的团队
export async function getMygroup(page, id, type) {
    return await request({
        url: `/my_groups?page=${page}&&open_id=${id}&&type=${type}`,
        method: 'get'
    })
}
// 提现
export async function cashOutto(data) {
    return await request({
        url: "/cash_out?money=" + data,
        method: 'get'
    })
}
// 修改信息
export async function updUserInfo(data) {

    return await request({
        url: "/complete_info",
        method: 'post',
        data: JSON.stringify(data)
    })
}
// 获取信息
export async function getUserInfo(data) {
    return await request({
        url: "/extension_info",
        method: 'get',
        data
    })
}
// 文章详情
export async function getActicleDetail(data) {
    return await request({
        url: "/article_detail",
        method: 'get',
        data
    })
}
// 文章列表
export async function getActicleList(data) {
    return await request({
        url: "/article_list",
        method: 'get',
        data
    })
}

// 文章分类
export async function getActicleType(data) {
    return await request({
        url: "/article_type",
        method: 'get',
        data
    })
}

// 海报
export async function getPosters(data) {
    return await request({
        url: "/posters",
        method: 'get',
        data
    })
}
// 提现
export async function cashOut(data) {
    return await request({
        url: "/cash_out",
        method: 'post',
        data
    })
}
// 客户
export async function getCustoms(type, page) {


    return await request({
        url: `/customs?date_type=${type}&&page=${page}`,
        method: 'get',

    })
}
// 收益
export async function getProfit(data) {
    return await request({
        url: "/profit",
        method: 'get',
        data
    })
}
// 收益
export async function is_login(data) {
    return await request({
        url: "/check",
        method: 'get',
        data
    })
}

// 获取openid
export async function getOpenId(code) {
    return await request({
        url: `/sign_in?code=${code}`,
        method: 'get',
    })
}
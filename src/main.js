/*
 * @Author: your name
 * @Date: 2020-06-11 14:47:57
 * @LastEditTime: 2020-07-29 17:39:24
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \distrbute\src\main.js
 */ 
import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import './assets/font/iconfont.css'
// import Vue from 'vue'
import{setCookie,getCookie} from './utils/utils'





import { Button, Tab, Tabs,List,Grid,GridItem,
  CouponCell, CouponList,Card ,Field,Cell, CellGroup,
  AddressList,AddressEdit ,Progress ,ActionSheet,Toast ,Loading ,NumberKeyboard ,
  Checkbox, CheckboxGroup,Popup,CountDown ,Overlay,Step, Steps,Divider ,Form ,Icon} from 'vant'
// Vue.use(List);
Vue.use(NumberKeyboard);
Vue.use(Loading);
  Vue.use(Icon);
  Vue.use(Form);
Vue.use(Divider);
Vue.use(Button)
Vue.use(Tab);
Vue.use(Tabs);
Vue.use(List);
Vue.use(Grid);
Vue.use(GridItem);
Vue.use(CouponCell);
Vue.use(CouponList);
Vue.use(Card);
Vue.use(Field);
Vue.use(Cell);
Vue.use(CellGroup);
Vue.use(AddressList);
Vue.use(AddressEdit);
Vue.use(Progress);
Vue.use(ActionSheet);
Vue.use(Toast);
Vue.use(Checkbox);
Vue.use(CheckboxGroup);
Vue.use(Popup);
Vue.use(CountDown);
Vue.use(Overlay);
Vue.use(Step);
Vue.use(Steps);
Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
router.beforeEach(async (to, from, next) => {
  store.commit('updateLoadingStatus', {isLoading: true})
 
  if (to.meta.title) {
    document.title = to.meta.title
    next()
  }else{
    next()
  }

})
/*
 * @Author: your name
 * @Date: 2020-06-11 14:47:57
 * @LastEditTime: 2020-07-31 11:45:40
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \distrbute\vue.config.js
 */ 
let baseUrl = 'sales'

module.exports = {
   publicPath:process.env.NODE_ENV === 'production' ? `/${baseUrl}/`:'/',
  //基本路径
  // publicPath: './',//默认的'/'是绝对路径，如果不确定在根路径，改成相对路径'./'
  // 输出文件目录
  outputDir: 'dist',
  assetsDir:'static',
  indexPath:'index.html',
  chainWebpack: config => {
    config
      .plugin('html')
      .tap(args => {
        args[0].title = '超维编程大使'
        return args
      })
    },
  // eslint-loader 是否在保存的时候检查
  lintOnSave: true,
  // 生产环境是否生成 sourceMap 文件
  productionSourceMap: false,
  // css相关配置
  css: {
  // 是否使用css分离插件 ExtractTextPlugin
  extract: true,
  // 开启 CSS source maps?
  sourceMap: false,
  },
  // webpack-dev-server 相关配置

  // 第三方插件配置
  pluginOptions: {
  // ...
  }
  };